//! Huffman compression
//! ```
//! let input = b"hello world";
//! let tree = huffman::Tree::from_iter(input);
//! println!("{:#?}", tree);
//! ```

use std::{borrow::Borrow, collections::HashMap, io, io::Read, io::Write, iter::FromIterator};

struct ByteFrequences([u32; 256]);

impl ByteFrequences {
	fn new() -> Self {
		Self([0; 256])
	}
	fn load<U: Borrow<u8>, T: IntoIterator<Item = U>>(&mut self, iter: T) -> usize {
		iter.into_iter()
			.map(|byte| self.0[*byte.borrow() as usize] += 1)
			.count()
	}
}

impl<T: Borrow<u8>> FromIterator<T> for ByteFrequences {
	fn from_iter<U>(iter: U) -> Self
	where
		U: IntoIterator<Item = T>,
	{
		let mut frequences = [0; 256];
		for byte in iter {
			frequences[*byte.borrow() as usize] += 1;
		}
		Self(frequences)
	}
}

/// Tree associating all occuring byte with a binary path
#[derive(Debug)]
pub enum Tree {
	Leaf {
		byte: u8,
		frequence: u32,
	},
	Node {
		weight: u32,
		left: Box<Tree>,
		right: Box<Tree>,
	},
}

impl Tree {
	pub fn from_iter<U: Borrow<u8>, T: IntoIterator<Item = U>>(iter: T) -> Option<(usize, Self)> {
		let mut freqs = ByteFrequences::new();
		let count = freqs.load(iter);
		if let Some(tree) = freqs.into() {
			Some((count, tree))
		} else {
			None
		}
	}
	fn weight(&self) -> u32 {
		match self {
			Self::Leaf { frequence, .. } => *frequence,
			Self::Node { weight, .. } => *weight,
		}
	}
	fn fill_transpose_table(&self, stack: &mut Vec<bool>, table: &mut HashMap<u8, Vec<bool>>) {
		match self {
			Self::Leaf { byte, .. } => {
				table.insert(*byte, stack.clone());
			}
			Self::Node { left, right, .. } => {
				stack.push(false);
				left.fill_transpose_table(stack, table);
				stack.pop();

				stack.push(true);
				right.fill_transpose_table(stack, table);
				stack.pop();
			}
		}
	}
	pub fn get_transpose_table(&self) -> TransposeTable {
		let mut hash_map = HashMap::new();
		let mut stack = Vec::new();
		self.fill_transpose_table(&mut stack, &mut hash_map);
		TransposeTable(hash_map)
	}
	pub fn serialize_as_bits<T: Write>(&self, output: &mut BitWriter<T>) -> io::Result<()> {
		match self {
			Self::Leaf { byte, .. } => {
				output.write_bit(false)?;
				output.write_byte(*byte)?;
			}
			Self::Node { left, right, .. } => {
				output.write_bit(true)?;
				left.serialize_as_bits(output)?;
				right.serialize_as_bits(output)?;
			}
		}
		Ok(())
	}
	pub fn deserde<T: Read>(input: &mut BitReader<T>) -> io::Result<Self> {
		if input.read_bit()? {
			Ok(Self::Node{
				left: Box::new(Self::deserde(input)?),
				right: Box::new(Self::deserde(input)?),
				weight: 0,
			})
		}
		else {
			Ok(Self::Leaf{byte: input.read_byte()?, frequence: 0})
		}
	}
	pub fn decode_byte<T: Read>(&self, input: &mut BitReader<T>) -> io::Result<u8> {
		match self {
			Self::Leaf{byte, ..} => Ok(*byte),
			Self::Node{left, right, ..} => {
				if input.read_bit()? {
					right
				}
				else {
					left
				}.decode_byte(input)
			}
		}
	}
}

impl std::ops::Add for Tree {
	type Output = Self;
	fn add(self, rhs: Self) -> Self {
		Self::Node {
			weight: self.weight() + rhs.weight(),
			left: Box::new(self),
			right: Box::new(rhs),
		}
	}
}

impl From<ByteFrequences> for Option<Tree> {
	fn from(ByteFrequences(frequences): ByteFrequences) -> Self {
		fn tree_cmp(lhs: &Tree, rhs: &Tree) -> std::cmp::Ordering {
			lhs.weight().cmp(&rhs.weight()).reverse()
		}
		fn builder(mut queue: Vec<Tree>) -> Option<Tree> {
			match (queue.pop(), queue.pop()) {
				(Some(lhs), Some(rhs)) => {
					let fused = lhs + rhs;
					queue.insert(
						match queue.binary_search_by(|e| tree_cmp(e, &fused)) {
							Ok(index) => index,
							Err(index) => index,
						},
						fused,
					);
					builder(queue)
				}
				(result, _) => result,
			}
		}

		let mut queue: Vec<_> = frequences
			.iter()
			.zip(0..=u8::MAX)
			.filter(|(&freq, _)| freq > 0)
			.map(|(&frequence, byte)| Tree::Leaf { byte, frequence })
			.collect();
		queue.sort_unstable_by(tree_cmp);
		builder(queue)
	}
}

/// Stores all binary paths
#[derive(Debug)]
pub struct TransposeTable(HashMap<u8, Vec<bool>>);

impl TransposeTable {
	pub fn get(&self, byte: u8) -> Option<&Vec<bool>> {
		self.0.get(&byte)
	}
}

impl From<Option<Tree>> for TransposeTable {
	fn from(tree: Option<Tree>) -> Self {
		TransposeTable(match tree {
			Some(tree) => {
				let mut result = HashMap::new();
				tree.fill_transpose_table(&mut Vec::new(), &mut result);
				result
			}
			None => HashMap::new(),
		})
	}
}

/// Wraps a writer to enable single bit write (big-endian)
pub struct BitWriter<'a, T: Write> {
	output: &'a mut T,
	buffer: u8,
	remain: usize,
}

impl<'a, T: Write> BitWriter<'a, T> {
	pub fn new(inner: &'a mut T) -> Self {
		Self {
			output: inner,
			buffer: 0,
			remain: 8,
		}
	}
	pub fn write_bit(&mut self, bit: bool) -> io::Result<()> {
		if self.remain == 0 {
			self.output.write_all(&[self.buffer])?;
			self.remain = 8;
			self.buffer = 0;
		}
		self.remain -= 1;
		self.buffer += (bit as u8) << self.remain;
		Ok(())
	}
	pub fn write_byte(&mut self, byte: u8) -> io::Result<()> {
		for shift in (0..8).rev() {
			self.write_bit((byte & (1 << shift)) != 0)?
		}
		Ok(())
	}
	pub fn terminate(self) -> io::Result<()> {
		if self.remain != 8 {
			self.output.write_all(&[self.buffer])?
		}
		Ok(())
	}
}

pub struct BitReader<'a, T: Read> {
	input: &'a mut T,
	buffer: u8,
	remain: usize,
}

impl<'a, T: Read> BitReader<'a, T> {
	fn new(inner: &'a mut T) -> Self {
		Self {
			input: inner,
			buffer: 0,
			remain: 0,
		}
	}
	fn read_bit(&mut self) -> io::Result<bool> {
		if self.remain == 0 {
			self.input.read_exact(std::slice::from_mut(&mut self.buffer))?;
			self.remain = 8;
		}
		self.remain -= 1;
		Ok((self.buffer & (1 << self.remain)) != 0)
	}
	fn read_byte(&mut self) -> io::Result<u8> {
		let mut result = 0;
		for shift in (0..8).rev() {
			result += (self.read_bit()? as u8) << shift;
		}
		Ok(result)
	}
	fn terminate(mut self) -> io::Result<()> {
		while self.remain != 0 {
			self.remain -= 1;
			if (self.buffer & (1 << self.remain)) == 1 {
				return Err(io::ErrorKind::InvalidData.into())
			}
		}
		Ok(())
	}
}

pub fn encode_pipeline<I, O>(input: &mut I, output: &mut O, chunk_size: u32) -> io::Result<()>
where
	I: Read,
	O: Write,
{
	let mut output = io::BufWriter::new(output);
	let mut buffer = Vec::with_capacity(1024);
	while let Some((count, tree)) = {
		buffer.clear();
		input.take(chunk_size as u64).read_to_end(&mut buffer)?;
		Tree::from_iter(&buffer)
	} {
		output.write_all(&[0x01])?;
		output.write_all(&(count as u32).to_be_bytes())?;
		let mut bit_writer = BitWriter::new(&mut output);
		tree.serialize_as_bits(&mut bit_writer)?;

		let transpose_table = tree.get_transpose_table();
		for byte in &buffer {
			for bit in transpose_table.get(*byte).unwrap() {
				bit_writer.write_bit(*bit)?;
			}
		}

		bit_writer.terminate()?;
	}
	output.write_all(&[0x00])?;
	output.flush()?;
	Ok(())
}

pub fn decode_pipeline<I, O>(input: &mut I, output: &mut O) -> io::Result<()>
	where I: Read, O: Write
{
	let mut buffer = [0];
	input.read_exact(&mut buffer)?;
	match buffer[0] {
		0x00 => Ok(()),
		0x01 => {
			let mut buffer = [0; 4];
			input.read_exact(&mut buffer)?;
			let count = u32::from_be_bytes(buffer);
			let mut bit_reader = BitReader::new(input);
			let tree = Tree::deserde(&mut bit_reader)?;
			for _ in 0..count {
				output.write_all(&[tree.decode_byte(&mut bit_reader)?])?;
			}
			bit_reader.terminate()?;
			decode_pipeline(input, output)
		}
		_ => Err(io::ErrorKind::InvalidData.into())
	}
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_pipeline() {
		let mut encoded = Vec::new();
		encode_pipeline(&mut &b"hello world"[..], &mut encoded, u32::MAX).unwrap();
		let mut decoded = Vec::new();
		decode_pipeline(&mut encoded.as_slice(), &mut decoded).unwrap();
		assert_eq!(b"hello world".as_ref(), decoded.as_slice());
	}

	#[test]
	fn test_bit_writer() {
		let mut result = Vec::new();
		let mut bit_writer = BitWriter::new(&mut result);
		bit_writer.write_bit(false).unwrap();
		bit_writer.write_bit(true).unwrap();
		bit_writer.write_byte(0b1100_1001).unwrap();
		bit_writer.terminate().unwrap();

		for byte in &result {
			println!("->{:08b}", *byte);
		}

		assert_eq!(result, vec![0b01_1100_10, 0b01_000000]);
	}

	#[test]
	fn test_bit_reader() {
		let mut input = [0b01_1100_00, 0b01_000000].as_ref();
		let mut bit_reader = BitReader::new(&mut input);
		assert_eq!(bit_reader.read_bit().unwrap(), false);
		assert_eq!(bit_reader.read_bit().unwrap(), true);
		assert_eq!(bit_reader.read_byte().unwrap(), 0b1100_0001);
		bit_reader.terminate().unwrap();
	}
}
