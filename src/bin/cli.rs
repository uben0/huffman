fn main() {
	match std::env::args().skip(1).next().unwrap().as_str() {
		"encode" => {
			let stdin_lock = std::io::stdin();
			let stdout_lock = std::io::stdout();
			huffman::encode_pipeline(&mut stdin_lock.lock(), &mut stdout_lock.lock(), u32::MAX).unwrap();
		}
		"decode" => {
			let stdin_lock = std::io::stdin();
			let stdout_lock = std::io::stdout();
			huffman::decode_pipeline(&mut stdin_lock.lock(), &mut stdout_lock.lock()).unwrap();
		}
		_ => {
			panic!("invalid mod, either 'encode' or 'decode'");
		}
	}
}