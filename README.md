# Huffman compression implementation in Rust

Can be use as a Rust crate (library) or as UNIX pipeline command:
```sh
# ENCODE
echo Hello World! | huffman encode > out.huf

# DECODE
cat out.huf | huffman decode
```
